import React from "react";
import { Platform, Text,View} from "react-native";
import {
  TabNavigator,
  StackNavigator,
  DrawerNavigator
} from "react-navigation";
import { FontAwesome, Ionicons } from "@expo/vector-icons";
import WelcomeScreen from "./screens/Welcome";
import HomeScreen from "./screens/Home";
import ProfileScreen from "./screens/Profile";
import ServicesScreen from "./screens/Services";
import SettingsScreen from "./screens/Settings";

import { HamburgerIcon, SettingsIcon, BackIcon } from "./components/icons";

import { CustomDrawerContent } from "./components";
import { colors } from "./utils/constants";

const AppMainTab = TabNavigator(
  {
    // Profile: {
    //   screen: ProfileScreen,
    //   navigationOptions: ({ navigation }) => ({
    //     drawerLabel: "الملف الشخصي",
    //     drawerIcon: ({ tintColor }) =>
    //       <FontAwesome name="user-circle" size={23} color={tintColor} />,
    //     tabBarLabel: "الملف الشخصي",
    //     tabBarIcon: ({ tintColor }) =>
    //       <FontAwesome name="user-circle" size={23} color={tintColor} />,
    //     headerStyle: {
    //       backgroundColor: colors.BLUE_100
    //     },
    //     headerTitle: "الملف الشخصي",
    //     headerTitleStyle: {
    //       color: colors.WHITE,
    //       textAlign: "right"
    //     },
    //     headerRight: (
    //       <HamburgerIcon onPress={() => navigation.navigate("DrawerOpen")} />
    //     )
    //   })
    // },
    Services: {
      screen: ServicesScreen,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: () => <Text style = {{color:"#FFF"}}>خدمات</Text>,
        drawerIcon: () =>
          <Ionicons name="ios-more" size={23} color={"#FFF"} />,
        tabBarLabel: "خدمات",
        tabBarIcon: () =>
          <Ionicons name="ios-more" size={23} color={"#FFF"} />,
        headerStyle: {
          backgroundColor: colors.BLUE_100,
        },
        headerTitle: "خدمات",
        headerTitleStyle: {
          color: colors.WHITE,
        },
        headerRight: (
          <HamburgerIcon navigation={navigation} />
        )
      })
    },
    Home: {
      screen: HomeScreen,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: () => <Text style = {{color:"#e74c3c", fontSize: 20, padding: 9}}>الرئيسية</Text>,
        drawerIcon: () =>
          <FontAwesome name="home" size={23} color={"#FFF"} />,
        tabBarLabel: "الأساسية",
        tabBarIcon: () =>
          <FontAwesome name="home" size={23} color={"#FFF"} />,
        headerStyle: {
          backgroundColor: colors.BLUE_100,
        },
        headerTitle: "الرئيسية",
        headerTitleStyle: {
          color: "white",
        },
        headerRight: (
          <HamburgerIcon navigator={navigation} />
        )
      })
    }
  },
  {
    tabBarOptions: {
      activeTintColor: colors.WHITE,
      inactiveTintColor: colors.BLUE_50,
      inactiveBackgroundColor: colors.BLUE_100,
      activeBackgroundColor: colors.BLUE_100,
      showIcon: true,
      showLabel: Platform.OS === "ios",
      indicatorStyle: {
        backgroundColor: colors.BLUE_300
      },
      style: {
        backgroundColor: colors.BLUE_100
      },
      upperCaseLabel: false
    },
    tabBarPosition: "bottom",
    swipeEnabled: false,
    animationEnabled: false
  }
);

const AppMainStack = StackNavigator(
  {
    Home: { screen: AppMainTab },
    Settings: { screen: HomeScreen }
  },
  {
    cardStyle: {
      backgroundColor: colors.BLUE_50
    },
    mode: "modal"
  }
);

const AppDrawer = DrawerNavigator(
  {
    Home: {
      screen: AppMainStack
    },
    // Settings: {
    //   screen: SettingsScreen,
    //   navigationOptions: ({ navigation }) => ({
    //     drawerLabel: () => <Text style = {{color:"#FFF"}}>الإعدادات</Text>,
    //     drawerIcon: () =>
    //       <Ionicons name="md-settings" size={23} color={"#FFF"} />,
    //     headerStyle: {
    //       backgroundColor: colors.BLUE_100
    //     },
    //     headerTitle: "الإعدادات",
    //     headerTitleStyle: {
    //       color: colors.WHITE
    //     },
    //     headerLeft: <BackIcon onPress={() => navigation.goBack()} />
    //   })
    // }
  },
  {
    drawerPosition: "right",
    drawerBackgroundColor: "#2d3436"
  },
  {
    contentComponent: props => <CustomDrawerContent {...props} />,
    contentOptions: {
      activeBackgroundColor: colors.BLUE_100,
      activeTintColor: colors.WHITE,
      inactiveTintColor: colors.BLUE_200,
      labelStyle: {color: 'white',}
    }
  }
);

const Navigator = TabNavigator(
  {
    // Welcome: { screen: WelcomeScreen },
    Main: { screen: AppDrawer }
  },
  {
    navigationOptions: {
      tabBarVisible: false
    },
    swipeEnabled: false
  }
);

export default Navigator;
