import React from "react";
import styled from "styled-components/native";
import { Ionicons } from "@expo/vector-icons";


const IconRightContainer = styled.TouchableOpacity`
  height: 100%;
  paddingRight: 15;
  justifyContent: center;
`;

const Hamburger = ({navigator}) =>
  <IconRightContainer>
    <Ionicons name="ios-menu" size={25} color="white" />
  </IconRightContainer>;

export default Hamburger;
