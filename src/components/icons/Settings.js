import React from "react";
import styled from "styled-components/native";
import { Ionicons } from "@expo/vector-icons";

const IconLeftContainer = styled.TouchableOpacity`
  height: 100%;
  paddingLeft: 15;
  justifyContent: center;
`;

const Settings = ({ onPress }) =>
  <IconLeftContainer onPress={onPress}>
    <Ionicons name="md-settings" size={25} color="white" />
  </IconLeftContainer>;

export default Settings;
