import React, { Component } from "react";
import styled from "styled-components/native";

import Icon from "react-native-vector-icons/FontAwesome";

const ContainerView = styled.View`
  flex: 1;
  justifyContent: center;
  alignItems: center;
`;

const TitleText = styled.Text`
  fontSize: 30;
  color: ${props => props.theme.WHITE};
`;

class HomeScreen extends Component {
  render() {
    return (
      <ContainerView>
        <Icon
          name="phone"
          reverse
          color="#e74c3c"
          style={{
            fontSize: 30,
            padding: 50,
            borderRadius: 10,
            borderColor: "#e74c3c",
            borderWidth: 2
          }}
          onPress={() => this.props.navigation.navigate("Services")}
        />
      </ContainerView>
    );
  }
}

export default HomeScreen;
