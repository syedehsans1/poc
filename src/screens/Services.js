import React, { Component } from "react";
import styled from "styled-components/native";
import { View, Text, Alert } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import MaterialIcons from "@expo/vector-icons/MaterialIcons";

const ContainerView = styled.View`
  flex: 1;
  justifyContent: center;
  alignItems: center;
`;

const TitleText = styled.Text`
  fontSize: 30;
  color: ${props => props.theme.WHITE};
`;
const Style = {
  fontSize: 35,
  padding: 30,
  marginBottom: -20
};
const ContainerStyle = {
  borderWidth: 1,
  borderColor: "black"
};
const textStyle = {
  marginBottom: 20
}
class ServicesScreen extends Component {
  render() {
    return (
      <View style={{ flexDirection: "column", flex: 1 }}>
        <View style={{ flexDirection: "row", flex: 1 }}>
          <ContainerView style={ContainerStyle}>
            <MaterialIcons
              name="security"
              reverse
              color="#e74c3c"
              style={Style}
              onPress={() => Alert.alert("محزر", "بوابة أمن واحد")}
            />
            <Text style = {textStyle}>بوابة أمن واحد</Text>
          </ContainerView>
          <ContainerView style={ContainerStyle}>
            <Icon
              name="home"
              reverse
              color="#e74c3c"
              style={Style}
              onPress={() => this.props.navigation.navigate("HomeScreen")}
            />
            <Text style = {textStyle}>الرئيسية</Text>
          </ContainerView>
        </View>
        <View style={{ flexDirection: "row", flex: 1 }}>
          <ContainerView style={ContainerStyle}>
            <MaterialIcons
              name="security"
              reverse
              color="#e74c3c"
              style={Style}
              onPress={() => Alert.alert("محزر", "بوابة الأمن الثلاثة")}
            />
            <Text style = {textStyle}>بوابة الأمن الثلاثة</Text>
          </ContainerView>
          <ContainerView style={ContainerStyle}>
            <MaterialIcons
              name="security"
              reverse
              color="#e74c3c"
              style={Style}
              onPress={() => Alert.alert("محزر", "بوابة الأمن اثنين")}
            />
            <Text style = {textStyle}>بوابة الأمن اثنين</Text>
          </ContainerView>
        </View>
        <View style={{ flexDirection: "row", flex: 1 }}>
          <ContainerView style={ContainerStyle}>
            <Icon
              name="group"
              reverse
              color="#e74c3c"
              style={Style}
              onPress={() => Alert.alert("محزر", "مكالمة جماعية")}
            />
            <Text style = {textStyle}>مكالمة جماعية</Text>
          </ContainerView>
          <ContainerView style={ContainerStyle}>
            <Icon
              name="volume-control-phone"
              reverse
              color="#e74c3c"
              style={Style}
              onPress={() => Alert.alert("محزر", "مكالمة طوارئ المجموعة")}
            />
            <Text style = {textStyle}>مكالمة طوارئ المجموعة</Text>
          </ContainerView>
        </View>
        <View style={{ flexDirection: "row", flex: 1 }}>
          <ContainerView style={ContainerStyle}>
            <MaterialIcons
              name="email"
              reverse
              color="#e74c3c"
              style={Style}
              onPress={() => Alert.alert("محزر", "البريد الإلكتروني")}
            />
            <Text style = {textStyle}>البريد الإلكتروني</Text>
          </ContainerView>
          <ContainerView style={ContainerStyle}>
            <Icon
              name="file-video-o"
              reverse
              color="#e74c3c"
              style={Style}
              onPress={() => Alert.alert("محزر", "مكالمة مجموعة الفيديو")}
            />
            <Text style = {textStyle}>مكالمة مجموعة الفيديو</Text>
          </ContainerView>
        </View>
      </View>
    );
  }
}

export default ServicesScreen;
