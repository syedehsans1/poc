export const colors = {
  WHITE: "#FFFFFF",
  BLUE_50: "#FFF",
  BLUE_100: "#2d3436",
  BLUE_200: "#2d3436",
  BLUE_300: "#2d3436",
  BLUE_400: "#2d3436",
  BLUE_500: "#2d3436"
};
